namespace Cards
{
	class PlayingCard
	{
        private readonly Suit _suit;
        private readonly Value _value;

		public PlayingCard(Suit s, Value v)
		{
			this._suit = s;
			this._value = v;
		}

        public override string ToString()
		{
            string result = $"{this._value} of {this._suit}";
            return result;
		}

        public Suit CardSuit()
        {
            return this._suit;
        }

        public Value CardValue()
        {
            return this._value;
        }
	}
}